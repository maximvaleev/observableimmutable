﻿/*Домашнее задание
Observable, Immutable и Concurrent коллекции

(1) Напишите программу "Постоянный покупатель" с двумя классами:
    Shop (Магазин)
    Customer (Покупатель)
    В классе Shop должна храниться информация о списке товаров (экземпляры классов Item). Также в классе Shop должны 
        быть методы Add (для добавления товара) и Remove (для удаления товара).
    В классе Item должны быть свойства Id (идентификатор товара) и Name (название товара).
    В классе Customer должен быть метод OnItemChanged, который будет срабатывать, когда список товаров в магазине обновился. 
        В этом методе надо выводить в консоль информацию о том, какое именно изменение произошло (добавлен товар 
        с таким-то названием и таким-то идентификатором / удален такой-то товар).
    В основном файле программы создайте Магазин, создайте Покупателя. Реализуйте через ObservableCollection возможность 
        подписки Покупателем на изменения в ассортименте Магазина - все изменения сразу должны отображаться в консоли 
        (должен срабатывать метод Customer.OnItemChanged).
    По нажатии клавиши A добавляйте новый товар в магазин. Товар должен называться "Товар от <текущее дата и время>", 
        где вместо <текущее дата и время> подставлять текущую дату и время.
    По нажатии клавиши D спрашивайте какой товар надо удалить. Пользователь должен ввести идентификатор товара, после чего 
        товар необходимо удалить из ассортимента магазина.
    По нажатии клавиши X выходите из программы.
    Добавьте в Магазин несколько товаров, удалите какие-то из них - убедитесь, что сообщения выводятся в консоль.
*/

namespace Shop;

internal class Program
{
    static void Main()
    {
        Shop shop = new("У Васяна");
        Customer fedya = new("Федя");
        Customer agafya = new("Агафья");

        shop.SubscribeCustomer(fedya.OnItemChanged);
        shop.SubscribeCustomer(agafya.OnItemChanged);

        OpenShop(shop);

        shop.UnSubscribeCustomer(fedya.OnItemChanged);
        shop.UnSubscribeCustomer(agafya.OnItemChanged);
    }

    private static void OpenShop(Shop shop)
    {
        ConsoleKeyInfo input;
        do
        {
            Console.Write("\n> Cоздание товара: A, удаление: D, выход: Х\n>>> ");
            input = Console.ReadKey();
            switch (input.Key)
            {
                case ConsoleKey.A:
                    Console.WriteLine();
                    shop.Add(new Item());
                    break;
                case ConsoleKey.D:
                    Console.Write("\nВведите ID товара для удаления: ");
                    string idInput = Console.ReadLine()!;
                    bool parseResult = int.TryParse(idInput, out int idToRemove);
                    if (parseResult)
                    {
                        shop.Remove(idToRemove);
                    }
                    else
                    {
                        Console.WriteLine("Неверный ввод!");
                    }
                    break;
            }
        } 
        while (input.Key != ConsoleKey.X);
    }
}