﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace House
{
    internal class Part4 : IPoemPart
    {
        public ImmutableList<string>? Poem { get; private set; }

        public string PoemPart { get; }

        public Part4()
        {
            PoemPart = "\n\nВот кот," +
                "\nКоторый пугает и ловит синицу," +
                "\nКоторая часто ворует пшеницу," +
                "\nКоторая в темном чулане хранится" +
                "\nВ доме," +
                "\nКоторый построил Джек.";
        }

        public ImmutableList<string> AddPart(ImmutableList<string>? prevPart)
        {
            if (prevPart != null)
            {
                Poem = prevPart.Add(PoemPart);
            }
            else
            {
                Poem = ImmutableList.Create(PoemPart);
            }
            return Poem;
        }
    }
}
