﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace House
{
    internal class Part1 : IPoemPart
    {
        public ImmutableList<string>? Poem { get; private set; }

        public string PoemPart { get; }

        public Part1()
        {
            PoemPart = "\n\nВот дом,\n" +
                "Который построил Джек.";
        }

        public ImmutableList<string> AddPart(ImmutableList<string>? prevPart)
        {
            if (prevPart != null)
            {
                Poem = prevPart.Add(PoemPart);
            }
            else
            {
                Poem = ImmutableList.Create(PoemPart);
            }
            return Poem;
        }
    }
}
