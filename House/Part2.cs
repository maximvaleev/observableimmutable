﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace House
{
    internal class Part2 : IPoemPart
    {
        public ImmutableList<string>? Poem { get; private set; }

        public string PoemPart { get; }

        public Part2()
        {
            PoemPart = "\n\nА это пшеница,\n" +
                "Которая в темном чулане хранится\n" +
                "В доме,\n" +
                "Который построил Джек.";
        }

        public ImmutableList<string> AddPart(ImmutableList<string>? prevPart)
        {
            if (prevPart != null)
            {
                Poem = prevPart.Add(PoemPart);
            }
            else
            {
                Poem = ImmutableList.Create(PoemPart);
            }
            return Poem;
        }
    }
}
