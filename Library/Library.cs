﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    internal class Library
    {
        private ConcurrentDictionary<string, int> _books;
        private CancellationTokenSource _cts;
        private Task _readingTask;

        public Library()
        {
            _books = new ConcurrentDictionary<string, int>();

            _cts = new CancellationTokenSource();
            _readingTask = ReadBooksAsync(_cts.Token);
        }

        public void AddBook(string name)
        {
            if (_books.TryAdd(name, 0))
            {
                Console.WriteLine($"Книга \"{name}\" добавлена успешно");
            }
            else
            {
                Console.WriteLine($"Не удалось добавить книгу \"{name}\"");
            }
        }

        public void ShowBooks()
        {
            foreach (KeyValuePair<string, int> item in _books)
            {
                Console.WriteLine($"* {item.Key} - {item.Value}%");
            }
        }

        private async Task ReadBooksAsync(CancellationToken token)
        {
            while (!token.IsCancellationRequested)
            {
                foreach (KeyValuePair<string, int> item in _books)
                {
                    if (item.Value < 100)
                    {
                        _books.TryUpdate(item.Key, item.Value + 1, item.Value);
                    }
                    else
                    {
                        Console.Write($"Книга {item.Key} прочитана. Удаление: ");
                        if (_books.TryRemove(item))
                        {
                            Console.WriteLine("успех");
                        }
                        else
                        {
                            Console.WriteLine("неудача");
                        }
                    }
                }
                try
                {
                    await Task.Delay(1000, token);
                }
                catch (TaskCanceledException)
                {
                    Console.WriteLine("\nПолучен запрос на остановку чтения книг");
                    // подавить исключение
                }
            }
        }

        // есть подозрение, что если не остановить бесконечный асинхронный метод, то GC экземпляры этого класса не соберет.
        public async Task StopReadingBooksAsync()
        {
            _cts.Cancel();
            await _readingTask;
            _cts.Dispose();
        }
    }
}
