﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop;

internal class Customer
{
    public string Name { get; }

    public Customer(string name = "Default Customer")
    {
        Name = name;
    }

    public void OnItemChanged(object? sender, NotifyCollectionChangedEventArgs e)
    {
        Console.WriteLine($"Покупатель {Name} получил уведомление:");
        switch (e.Action)
        {
            case NotifyCollectionChangedAction.Add:
                if (e.NewItems?[0] is Item newItem)
                    Console.WriteLine($"В магазин добавлен новый товар:\n\t Id = {newItem.Id}, Name = {newItem.Name}");
                break;
            case NotifyCollectionChangedAction.Remove:
                if (e.OldItems?[0] is Item oldItem)
                    Console.WriteLine($"Из магазина удален товар:\n\t Id = {oldItem.Id}, Name = {oldItem.Name}");
                break;
            default:
                Console.WriteLine($"Необработанное событие {e.Action}");
                break;
        }
    }
}
