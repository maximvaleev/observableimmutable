﻿/*
(3) Напишите программу "Дом, который построил Джек".
    В программе должна быть коллекция строк. Каждая строка - строка стихотворения "Дом, который построил Джек".
    Изначально коллекция пустая
    Также в программе есть 9 классов - Part1, Part2, Part3, ..., Part9
    В каждом классе PartN есть метод AddPart, который на вход принимает коллекцию строк, добавляет в нее новые строки и 
        сохраняет получившуюся коллекцию в свойство "Poem". Требуется это делать так, чтобы исходная коллекция 
        не изменилась. Какие именно строки добавляет каждый класс посмотрите здесь -
        https://russkaja-skazka.ru/dom-kotoryiy-postroil-dzhek-stihi-samuil-marshak/ 
        (например Part3 добавляет третий параграф стихотворения)
    Надо создать экземпляры этих классов, а затем последовательно вызвать каждый из методов AddPart, передавай в 
        него результат вызова предыдущего метода, примерно так: 'MyPart3(MyPart2.Poem)'
    В конце работы программы надо вывести значение свойства "Poem" у каждого из классов и убедиться, что изменяя коллекцию 
        в одном классе Вы не затрагивали коллекцию в предыдущем.
*/

using System.Collections.Immutable;

namespace House;

internal class Program
{
    static void Main()
    {
        ImmutableList<string> poem = ImmutableList.Create("Дом, который построил Джек");

        Part1 part1 = new();
        Part2 part2 = new();
        Part3 part3 = new();
        Part4 part4 = new();
        Part5 part5 = new();
        Part6 part6 = new();
        Part7 part7 = new();
        Part8 part8 = new();
        Part9 part9 = new();

        part1.AddPart(poem);
        part2.AddPart(part1.Poem);
        part3.AddPart(part2.Poem);
        part4.AddPart(part3.Poem);
        part5.AddPart(part4.Poem);
        part6.AddPart(part5.Poem);
        part7.AddPart(part6.Poem);
        part8.AddPart(part7.Poem);
        part9.AddPart(part8.Poem);

        Console.WriteLine(string.Join('\0', poem));
        Console.WriteLine("\n\t* * *\n");
        Console.WriteLine(string.Join('\0', part1.Poem!));
        Console.WriteLine("\n\t* * *\n");
        Console.WriteLine(string.Join('\0', part2.Poem!));
        Console.WriteLine("\n\t* * *\n");
        Console.WriteLine(string.Join('\0', part3.Poem!));
        Console.WriteLine("\n\t* * *\n");
        Console.WriteLine(string.Join('\0', part4.Poem!));
        Console.WriteLine("\n\t* * *\n");
        Console.WriteLine(string.Join('\0', part5.Poem!));
        Console.WriteLine("\n\t* * *\n");
        Console.WriteLine(string.Join('\0', part6.Poem!));
        Console.WriteLine("\n\t* * *\n");
        Console.WriteLine(string.Join('\0', part7.Poem!));
        Console.WriteLine("\n\t* * *\n");
        Console.WriteLine(string.Join('\0', part8.Poem!));
        Console.WriteLine("\n\t* * *\n");
        Console.WriteLine(string.Join('\0', part9.Poem!));
        Console.WriteLine("\n\t* * *\n");
    }
}