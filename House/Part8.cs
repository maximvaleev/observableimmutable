﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace House
{
    internal class Part8 : IPoemPart
    {
        public ImmutableList<string>? Poem { get; private set; }

        public string PoemPart { get; }

        public Part8()
        {
            PoemPart = "\n\nА это ленивый и толстый пастух," +
                "\nКоторый бранится с коровницей строгою," +
                "\nКоторая доит корову безрогую," +
                "\nЛягнувшую старого пса без хвоста," +
                "\nКоторый за шиворот треплет кота," +
                "\nКоторый пугает и ловит синицу," +
                "\nКоторая часто ворует пшеницу," +
                "\nКоторая в темном чулане хранится" +
                "\nВ доме," +
                "\nКоторый построил Джек.";
        }

        public ImmutableList<string> AddPart(ImmutableList<string>? prevPart)
        {
            if (prevPart != null)
            {
                Poem = prevPart.Add(PoemPart);
            }
            else
            {
                Poem = ImmutableList.Create(PoemPart);
            }
            return Poem;
        }
    }
}
