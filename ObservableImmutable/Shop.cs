﻿using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace Shop;

internal class Shop
{
    public string Name { get; set; }
    private readonly ObservableCollection<Item> _items;

	public Shop(string name = "DefaultShop")
	{
		Name = name;
        _items = new ObservableCollection<Item>();
        Console.WriteLine($"Создан магазин \"{Name}\".");
	}

    public void Add(Item item)
    {
        Console.WriteLine($"В магазин \"{Name}\" будет добавлен товар:\n\tId = {item.Id}, Name = {item.Name}");
        _items.Add(item);
    }

    public bool Remove(int itemId)
    {
        Item? item = _items.SingleOrDefault(x => x.Id == itemId);
        bool result = false;
        if (item is not null)
        {
            Console.WriteLine($"Из магазина \"{Name}\" будет удален товар:\n\tId = {item!.Id}, Name = {item.Name}");
            result = _items.Remove(item);
        }
        if (!result)
        {
            Console.WriteLine($"Не удалось удалить товар с ID = {itemId}");
        }
        return result;
    }

    public void SubscribeCustomer(NotifyCollectionChangedEventHandler collectionChangedHandler)
    {
        _items.CollectionChanged += collectionChangedHandler;
    }

    public void UnSubscribeCustomer(NotifyCollectionChangedEventHandler collectionChangedHandler)
    {
        _items.CollectionChanged -= collectionChangedHandler;
    }
}
