﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace House
{
    internal interface IPoemPart
    {
        public ImmutableList<string>? Poem { get; }
        public string PoemPart { get; }

        public ImmutableList<string> AddPart(ImmutableList<string>? prevPart);
    }
}
