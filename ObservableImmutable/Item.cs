﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop
{
    internal class Item
    {
        // предположим, что у нас будет создано менее 2 млрд. товаров)
        public static int CreatedCount { get; private set; } = 0;
        public int Id { get; }
        public string Name { get; set; }

        public Item(string? name = null)
        {
            Name = name is null? "Товар от " + DateTime.Now.ToLongTimeString() : name;
            Id = CreatedCount;
            CreatedCount++;
        }
    }
}
